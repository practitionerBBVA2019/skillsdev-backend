//Requires
var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

//Init variables
var express_var = express();
const bodyParser = require('body-parser');

var usuarioService = require('../services/usuario_service');

//middleware para habilitar el CORS
express_var.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,token");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
  });

//middleware: se ejecuta antes de la petición
express_var.use(bodyParser.urlencoded({extend:false}));
express_var.use(bodyParser.json());


  //:::::::::::::::::::::::::::::::::::::::::::::::
  //:::::Métodos para Usuario::::::
  //:::::::::::::::::::::::::::::::::::::::::::::::


  //Lista usuario por registro
  express_var.get('/usuario/:registro', (req, res) => {
    console.log('Atendiendo GET /usuario/:registro');
    var registro = req.params.registro;

      usuarioService.listarPorRegistro(registro,req, res).then(response =>{
          res.json(response);
      }).catch( error => {
          res.status( 500 ).send( error )
        });

  });

// Login
express_var.post('/login', (req, res) => {
  var body = req.body;
    //console.log("Password encrypted:"+bcrypt.hashSync(body.password, 10));
    usuarioService.listarPorRegistro(body, req, res).then(usuarioDB=>{
          if(usuarioDB.length<1){
              return res.status(400).json({
                  ok:false,
                  mensaje:'No existe usuario con el registro ingresado!'
              });
          }

      //Si el password ingresado no coincide con el de BD, termina execucion
      if(!bcrypt.compareSync(body.password,  usuarioDB[0].password)){
          return res.status(400).json({
              ok:false,
              mensaje:'Credenciales incorrectas'
          });
      }
      
      //Quita la contraseña para no enviarla al browser.
      usuarioDB[0].password = '*****';

       //Genera el token
      var token = jwt.sign({
          usuarioDB
      },process.env.SEED,{expiresIn: parseInt(process.env.TIME_EXP)});

      //Si logeo es exitoso devuelve el usuario
       return res.status(200).json({
          ok:true,
          usuario:usuarioDB,
          token:token,
          mensaje:'Logeo exitoso!'
      });

  }).catch( error => {
      res.status( 500 ).send( error )
    });

});

module.exports = express_var;
