//Requires
var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

const { tokenVerify } = require('../middlewares/autenticacion');
const cors = require('cors');
//Init variables
var express_var = express();
const bodyParser = require('body-parser');

var skillService = require('../services/skill_service');

//middleware para habilitar el CORS
express_var.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,token");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});

//middleware: se ejecuta antes de la petición
express_var.use(bodyParser.urlencoded({extend:false}));
express_var.use(bodyParser.json());

express_var.use(cors());
express_var.options('*', cors());

  //:::::::::::::::::::::::::::::::::::::::::::::::
  //::::::Métodos para skill::::::
  //:::::::::::::::::::::::::::::::::::::::::::::::

  // Lista todos las skills
  express_var.get('/skill', (req, res) => {
    console.log('Atendiendo GET /skill');
      skillService.listar(req, res).then(response =>{
          res.json(response.docs);
      }).catch( error => {
          res.status( 500 ).send( error )
        });
  });

    //Lista skill por Id
    express_var.post('/skill/:_id',tokenVerify, (req, res) => {
      console.log('Atendiendo GET /skill/:_id');
        var _id = req.body._id;
        skillService.listarPorId(_id,req, res).then(response =>{
            res.json(response);
        }).catch( error => {
            res.status( 500 ).send( error )
          });
    });

    //Lista skill por codigo_skill
    express_var.get('/skill/:codigo_skill', (req, res) => {
      console.log('Atendiendo GET /skill/:parametro');
        var parametro = req.params.parametro;
        skillService.listarPorNomDesc(parametro,req, res).then(response =>{
            res.json(response);
        }).catch( error => {
            res.status( 500 ).send( error )
          });
    });    

  //Lista skill por descripcion_skill
  express_var.get('/skill/:descripcion_skill', (req, res) => {
    console.log('Atendiendo GET /skill/:descripcion_skill');
    var descripcion_skill = req.params.descripcion_skill;

      skillService.listarPorDescripcion(descripcion_skill,req, res).then(response =>{
          res.json(response);
      }).catch( error => {
          res.status( 500 ).send( error )
        });

  });

//Crea nueva skill
  express_var.post('/skill',tokenVerify,(req, res)=>{
    console.log('Atendiendo POST /skill');
    var body = req.body;

    skillService.crear(body,req, res).then(response =>{
        res.json(response);
    }).catch( error => {
        res.status( 500 ).send( error )
      });

  });

  //Actualiza skill
  express_var.put('/skill/:codigo_skill',tokenVerify,(req,res)=>{
    console.log('Atendiendo PUT /skill/:codigo_skill');
    var body = req.body;
    skillService.actualizar(body,req, res).then(response =>{
        res.json(response);
    }).catch( error => {
        res.status( 500 ).send( error )
      });

  });

  //Elimina(logico) skill
  express_var.delete('/skill/:codigo_skill',tokenVerify,(req,res)=>{
    console.log('Atendiendo DELETE /skill/:codigo_skill');
    var body = req.body;
    skillService.eliminar(body,req, res).then(response =>{
        res.json(response);
    }).catch( error => {
        res.status( 500 ).send( error )
      });

  });


module.exports = express_var;
