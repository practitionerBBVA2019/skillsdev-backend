//Requires
var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

const { tokenVerify } = require('../middlewares/autenticacion');
const cors = require('cors');
//Init variables
var express_var = express();
const bodyParser = require('body-parser');

var personaService = require('../services/persona_service');
var personaSkillService = require('../services/persona_skill_services');
var personaProjectService = require('../services/persona_project_services');
var skillService = require('../services/skill_service');

//middleware para habilitar el CORS
express_var.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,token");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});

//middleware: se ejecuta antes de la petición
express_var.use(bodyParser.urlencoded({extend:false}));
express_var.use(bodyParser.json());

express_var.use(cors());
express_var.options('*', cors());

  //:::::::::::::::::::::::::::::::::::::::::::::::
  //::::::Métodos para Persona::::::
  //:::::::::::::::::::::::::::::::::::::::::::::::

  // Lista todos las personas
  express_var.get('/persona', (req, res) => {
    console.log('Atendiendo GET /persona');
      personaService.listar(req, res).then(response =>{
        console.log(response);
          res.json(response);
      }).catch( error => {
          res.status( 500 ).send( error )
        });
  });

  //Lista persona con sus skills por registro
  express_var.get('/persona/:registro', (req, res) => {
    console.log('Atendiendo GET /persona/:registro');
    var registro = req.params.registro;
    
    Promise.all([personaService.listarPorRegistro(registro,req, res), 
                personaSkillService.listarSkillsPorRegistro(registro,req, res)])
                .then(response => {
                  console.log('personas: '+response[0]);
                  console.log('personaskills: '+response[1]);
                  if(response[0].length==0 || response[1].length==0){
                    persona = "No se encontró persona para el registro: "+registro;
                    console.log(persona);
                    
                  }else{
                    persona = {
                      "registro":registro,
                      "nombres":response[0][0].nombres,
                      "unidad":response[0][0].unidad,
                      "disciplina":response[0][0].disciplina,
                      "building_block":response[0][0].building_block,
                      "nivel_techu":response[0][0].nivel_techu,
                      "skills":response[1][0].skills
                    }
                  }
                  
                  res.json(persona);
                });

  });    

//Crea nueva persona
  express_var.post('/persona',tokenVerify,(req, res)=>{
    console.log('Atendiendo POST /persona');
    var body = req.body;

    personaService.crear(body,req, res).then(response =>{
        res.json(response);
    }).catch( error => {
        res.status( 500 ).send( error )
      });
  });

  //Actualiza persona
  express_var.put('/persona/:registro',tokenVerify,(req,res)=>{
    console.log('Atendiendo PUT /persona/:registro');
    var body = req.body;
    personaService.actualizar(body,req, res).then(response =>{
        res.json(response);
    }).catch( error => {
        res.status( 500 ).send( error )
      });

  });

  //Elimina(logico) persona
  express_var.delete('/persona/:registro',tokenVerify,(req,res)=>{
    console.log('Atendiendo DELETE /persona/:registro');
    var body = req.body;
    personaService.eliminar(body,req, res).then(response =>{
        res.json(response);
    }).catch( error => {
        res.status( 500 ).send( error )
      });

  });

  //Asignar skill a persona.
  express_var.post('/persona/asignaskill',tokenVerify,(req, res)=>{
    console.log('Atendiendo POST /persona/asignaskill/');
    var body = req.body;

    skillService.listarPorCodigoSkill(body.codigo_skill,req, res).then(skillObject =>{
      console.log(body);
      return personaSkillService.asignaskillPorRegistro(body,skillObject, res);
    }).then(response =>{
      if(response==null) response = "No se econtró la persona con el registro enviado en personskill";
      res.json(response);
    }).catch( error => {
        res.status( 500 ).send( error )
      });

  });

   //Quitar skill a persona.
   express_var.post('/persona/quitarskill',tokenVerify,(req, res)=>{
    console.log('Atendiendo POST /persona/quitarskill/');
    var body = req.body;

    personaSkillService.quitarkillPorRegistro(body.registro,body.codigo_skill, res).then(response =>{
      res.json(response);
    }).catch( error => {
        res.status( 500 ).send( error )
      });
  }); 

   //Actualizar nivel skills
   express_var.put('/persona_skill/nivel',tokenVerify,(req, res)=>{
    console.log('Atendiendo PUT /persona_skill/nivel');
    var body = req.body;

    personaSkillService.actualizarNivelSkill(body, res).then(response =>{
      res.json(response);
    }).catch( error => {
        res.status( 500 ).send( error )
      });
  }); 


  //Lista persona y proyectos por registro
  express_var.get('/personaprojects/:registro', (req, res) => {
    console.log('Atendiendo GET /personaprojects/:registro');
    var registro = req.params.registro;
    
    Promise.all([personaService.listarPorRegistro(registro,req, res), 
                personaProjectService.listarProjectsPorRegistro(registro,req, res)])
                .then(response => {
                  console.log('personas: '+response[0]);
                  console.log('personaprojects: '+response[1]);
                  if(response[0].length==0 || response[1].length==0){
                    persona = "No se encontró persona para el registro solicitado."
                  }else{
                    persona = {
                      "registro":registro,
                      "nombres":response[0][0].nombres,
                      "unidad":response[0][0].unidad,
                      "disciplina":response[0][0].disciplina,
                      "building_block":response[0][0].building_block,
                      "nivel_techu":response[0][0].nivel_techu,
                      "proyectos":response[1][0].proyectos
                    }
                  }
                  
                  res.json(persona);

                }).catch( error => {
                  res.status( 500 ).send( error )
                });

  }); 

  //Lista personas por skill
  express_var.get('/personaskill', (req, res) => {
    console.log('Atendiendo GET /personaskill');

    personaSkillService.listarPorSkill(req, res).then(personaSkill =>{
      console.log(personaSkill);
      var promises = [];
      for (let i = 0; i < personaSkill.docs.length; i++) {
          promises.push(personaService.listarPorRegistro(personaSkill.docs[i].registro,req, res));
      }
      //return personaService.listarPorRegistro(body,skillObject, res);
      Promise.all(promises).then(personas => {
        console.log(personaSkill.docs);
        console.log(personas);
        /*
        for (let p = 0; p < personas.length; p++) {
          for (let s = 0; s < personaSkill.docs.length; s++) {
            if(personas[p].length>0 && personas[p][0]._doc.registro == personaSkill.docs[s].registro){
              personas[p][0]._doc.skills = personaSkill.docs[s].skills;
              break;
            }
          } 
        }
        */
       for (let s = 0; s < personas.length; s++) {
        for (let p = 0; p < personaSkill.docs.length; p++) {
          // if(personas[p].length==0){
          //   personaSkill.docs.splice(p,1);
          //   break
          // }else
          if(personas[p].length>0 && personas[p][0]._doc.registro == personaSkill.docs[s].registro){
            personaSkill.docs[s].nombres = personas[p][0]._doc.nombres;
            personaSkill.docs[s].apellido_paterno = personas[p][0]._doc.apellido_paterno;
            personaSkill.docs[s].apellido_materno = personas[p][0]._doc.apellido_materno;
            personaSkill.docs[s].unidad = personas[p][0]._doc.unidad;
            personaSkill.docs[s].disciplina = personas[p][0]._doc.disciplina;
            personaSkill.docs[s].building_block = personas[p][0]._doc.building_block;
            personaSkill.docs[s].nivel_techu = personas[p][0]._doc.nivel_techu;
            break;
          }
        } 
      }

      // for (let index = 0; index < personaSkill.docs.length; index++) {
      //   if(personaSkill.docs[index].nombres==undefined)
      //     personaSkill.docs.splice(index,1);
      // }
        res.json(personaSkill);
      }).catch( error => {
        console.log(error);
      });

    }).catch( error => {
      res.status( 500 ).send( error )
    });                
                

  });   

module.exports = express_var;
