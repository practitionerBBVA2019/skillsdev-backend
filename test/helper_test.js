const mongoose = require('mongoose');

before(done => {
    //const cadenaConexion = 'mongodb://localhost:27017/skilldb_local';
    mongoose.connect(process.env.STRING_CONECTION_DB);
    mongoose.connection
    .once("open", ()=>{
        console.log("conectado localmente a MongoDB test");
        done();
    })
    .on("error",err =>{
        console.error("error",err);
    })
});