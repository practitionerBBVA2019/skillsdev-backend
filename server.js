//Requires
require('dotenv').config();
var express = require('express');

//Init variables
var express_var = express();
const cors = require('cors');

var personaRoutes = require('./routes/persona_routes');
var usuarioRoutes = require('./routes/usuario_routes');
var skillRoutes = require('./routes/skill_routes');

//express_var.use(cors);
//express_var.options('*', cors());

//Rutas
express_var.use('/',usuarioRoutes);
express_var.use('/',personaRoutes);
express_var.use('/',skillRoutes);

//Listenning requests
var port = process.env.PORT || 3000;
express_var.listen(port,()=>{
    console.log('Express server puerto '+port);
});

module.exports = {
    express_var
}