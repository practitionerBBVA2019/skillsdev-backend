var Persona_projects = require('../models/persona_project_model');

exports.listarProjectsPorRegistro = function (registro,req,res){
    var regex = new RegExp(registro, "i");
    console.log(regex);
     return Persona_projects.find({"registro":{$regex:regex}}).then(
        persona_projects => persona_projects
    ).catch(error => {
        res.status(500).send(error);
    });
}
