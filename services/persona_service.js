var Persona = require('../models/persona_model');
const { mongoose } = require('../configurations/mongoose_config');

exports.listar = function (req,res){
    console.log(req.query);
    
    var limit = parseInt(req.query.limit);
    var page = (parseInt(req.query.page)<=0?parseInt(1):parseInt(req.query.page));
    var nombres = req.query.nombres;
    var regex = new RegExp(nombres, "i");
    var query = {
                    "estado": "1", 
                    $or:[
                        {"nombres":{$regex:regex}},
                        {"apellido_paterno":{$regex:regex}},
                        {"apellido_materno":{$regex:regex}}
                    ]

                };
    
    var aggregate = Persona.aggregate().match(query);  

      return Persona.aggregatePaginate(aggregate,{ page: page, limit: limit }).then(
            personas => personas
        ).catch(error => {
            res.status(500).send(error);
        });

}

exports.listarPorRegistro = function (registro,req,res){
    var regex = new RegExp(registro, "i");
    console.log(regex);
  return Persona.find({"registro":{$regex:regex},estado:"1"}).then(
          personas => personas
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.listarPorNombres = function (nombres,req,res){
    var regex = new RegExp(nombres, "i");
    console.log(regex);
  return Persona.find({"nombres":{$regex:regex},estado:"1"}).then(
          personas => personas
      ).catch(error => {
          res.status(500).send(error);
      });
    
}

exports.crear = function (body,req,res){
  var persona = new Persona({
    registro: 				    body.registro,
    password: 			      body.password,
    nombres: 			        body.nombres ,
    apellido_paterno: 	  body.apellido_paterno,
    apellido_materno: 	  body.apellido_materno,
    unidad: 	            body.unidad,
    area: 	              body.area,
    fecha_ingreso: 	      body.fecha_ingreso,
    funcion: 	            body.funcion,
    disciplina: 	        body.disciplina,
    especialidad: 	      body.especialidad,
    correo: 	            body.correo,
    building_block: 	    body.building_block,
    estado: 	            body.estado,
    perfil: 	            body.perfil,
    perfil_engineering: 	body.perfil_engineering,
    fecha_alta:				    body.fecha_alta,
    fecha_modificacion:   body.fecha_modificacion,
    usuario_modificacion: body.usuario_modificacion,
    usuario_alta: 			  body.usuario_alta
  });

  return persona.save().then(
          personaCreada => personaCreada
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.actualizar = function (body,req,res){
  var query = {registro:body.registro}
  return Persona.findOneAndUpdate(query,body,{useFindAndModify: false, new:true}).then(
          personaActualizada => personaActualizada
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.eliminar = function (body,req,res){
  var query = {registro:body.registro};
  body = {registro:body.registro,estado:'0'};
  return Persona.findOneAndUpdate(query,body,{useFindAndModify: false, new:true}).then(
          personaActualizada => personaActualizada
      ).catch(error => {
          res.status(500).send(error);
      });
}

