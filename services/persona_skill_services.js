var Persona_skills = require('../models/persona_skill_model');

exports.listarSkillsPorRegistro = function (registro,req,res){
    var regex = new RegExp(registro, "i");
    console.log(regex);
     return Persona_skills.find({"registro":{$regex:regex}}).then(
        persona_skills => persona_skills
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.asignaskillPorRegistro = function (body,skillObject,res){
    var regex = new RegExp(body.registro, "i");
    var query = {"registro":{$regex:regex}};
    var bodyUpdate = {
                        $push:{
                                "skills":skillObject
                            }
                    };
    return Persona_skills.findOneAndUpdate(query,bodyUpdate,{useFindAndModify: false, new:true}).then(
        personaSkillUpdated => personaSkillUpdated
        ).catch(error => {
            res.status(500).send(error);
        });
  }

  exports.quitarkillPorRegistro = function (registro, codigo_skill,res){
    var regex = new RegExp(registro, "i");
    var query = {"registro":{$regex:regex}};
    var bodyUpdate = {
                        $pull:{
                            "skills":{codigo_skill: codigo_skill}
                        }
                    };
    return Persona_skills.findOneAndUpdate(query,bodyUpdate,{useFindAndModify: false, new:true}).then(
        personaSkillUpdated => personaSkillUpdated
        ).catch(error => {
            res.status(500).send(error);
        });
  }

  exports.actualizarNivelSkill = function (body,res){
    var regex = new RegExp(body.registro, "i");
    var query = {
                    "registro":{$regex:regex}
                };

    var bodyUpdate = {
                        "$set":{
                            "skills.$[el].nivel_ingresado":body.nivel_ingresado,
                            "skills.$[el].nivel_valorado":body.nivel_valorado
                        }
                    };

    var options =   {
                        arrayFilters: [{ "el.codigo_skill": body.codigo_skill }],
                        useFindAndModify: false, 
                        new:true
                    }                    
    return Persona_skills.findOneAndUpdate(query,bodyUpdate,options).then(
        personaSkillUpdated => personaSkillUpdated
        ).catch(error => {
            res.status(500).send(error);
        });
  }

  exports.listarPorSkill = function (req,res){
    console.log(req.query);
    var limit = parseInt(req.query.limit);
    var page = (parseInt(req.query.page)<=0?parseInt(1):parseInt(req.query.page));
    var skill = req.query.skill;
    var regex = new RegExp(skill, "i");
    var query = { 
                    $or:[
                        {"skills.descripcion_skill":{$regex:regex}},
                        {"skills.detalle_skill":{$regex:regex}}
                    ]

                };
    
    var aggregate = Persona_skills.aggregate().match(query);  

      return Persona_skills.aggregatePaginate(aggregate,{ page: page, limit: limit }).then(
            personaSkill => personaSkill
        ).catch(error => {
            res.status(500).send(error);
        });

}  