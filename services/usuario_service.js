var Usuario = require('../models/usuario_model');

exports.listar = function (req,res){
  return Usuario.find({}).then(
          usuarios => usuarios
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.listarPorRegistro = function (body,req,res){
  var regex = new RegExp(body.registro, "i");
  return Usuario.find({"registro":{$regex:regex}}).then(
            usuarios => usuarios
      ).catch(error => {
          res.status(500).send(error);
      });
}




