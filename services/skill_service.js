var Skill = require('../models/skill_model');
const { mongoose } = require('../configurations/mongoose_config');

exports.listar = function (req,res){

     var limit = parseInt(req.query.limit);
     var page = (parseInt(req.query.page)<=0?parseInt(1):parseInt(req.query.page));
     var parametro = req.query.parametro;
     var regex = new RegExp(parametro, "i");
     var query = {"descripcion_skill":{$regex:regex},"detalle_skill":{$regex:regex},estado:"1"};
     
     var aggregate = Skill.aggregate().match(query);  
 
       return Skill.aggregatePaginate(aggregate,{ page: page, limit: limit }).then(
             skills => skills
         ).catch(error => {
             res.status(500).send(error);
         });     
}

exports.listarPorId = function (_id,req,res){
  return Skill.find({"_id":_id,estado:"1"}).then(
          skills => skills
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.listarPorCodigoSkill = function (codigo_skill,req,res){
    return Skill.find({"codigo_skill":codigo_skill,estado:"1"}).then(
            skills => skills
        ).catch(error => {
            res.status(500).send(error);
        });
  }

exports.listarPorNomDesc = function (parametro,req,res){
    var regex = new RegExp(parametro, "i");
    return Skill.find({"descripcion_skill":{$regex:regex},"detalle_skill":{$regex:regex},estado:"1"}).then(
            skills => skills
        ).catch(error => {
            res.status(500).send(error);
        });
  }  

exports.listarPorDescripcion = function (descripcion_skill,req,res){
    var regex = new RegExp(descripcion_skill, "i");
    console.log(regex);
  return Skill.find({"descripcion_skill":{$regex:regex},estado:"1"}).then(
          skills => skills
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.crear = function (body,req,res){
  var skill = new Skill({
    estado: 			      '1',
    descripcion_skill: 	      body.descripcion_skill ,
    detalle_skill:            body.detalle_skill
  });

  return skill.save().then(
          skillCreada => skillCreada
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.actualizar = function (body,req,res){
  var query = {_id:body._id}
  return Skill.findOneAndUpdate(query,body,{useFindAndModify: false, new:true}).then(
          skillActualizada => skillActualizada
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.eliminar = function (body,req,res){
  var query = {_id:body._id};
  body = {_id:body._id,estado:'0'};
  return Skill.findOneAndUpdate(query,body,{useFindAndModify: false, new:true}).then(
          skillActualizada => skillActualizada
      ).catch(error => {
          res.status(500).send(error);
      });
}
