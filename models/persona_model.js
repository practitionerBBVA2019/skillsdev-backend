var mongoose = require('mongoose');
var aggregatePaginate  = require('mongoose-aggregate-paginate-v2');

var Schema = mongoose.Schema;

var personaSchema = new Schema({
  registro:{type: String, require: [true, 'El registro es necesario']},
  password:{type: String, require:[true, 'El password es necesario']},
  nombres:{type: String, require:[true, 'El nombre es necesario']},
  apellido_paterno:{type: String, require:[true, 'El Apellido paterno es necesario']},
  apellido_materno:{type: String, require:[true, 'El Apellido materno es necesario']},
  unidad:{type: String, require:false},//descriptora
  area:{type: String, require:false},//descriptora
  fecha_ingreso:{type: Date, require:[true, 'La fecha de ingreso es necesaria']},//descriptora
  funcion:{type: String, require:[true, 'La funcion es necesaria']},//descriptora
  disciplina:{type: String, require:[true, 'La disciplina es necesaria']},//descriptora
  especialidad:{type: String, require:[true, 'La especialidad es necesaria']},
  correo:{type: String, require:[true, 'El correo es necesario']},
  building_block:{type: String, require:[true, 'El Building block es necesario']},//descriptora
  estado:{type: String, require:[true, 'La disciplina es necesaria']},//descriptora
  perfil:{type: String, require:[true, 'El Perfil es necesario']},//descriptora
  perfil_engineering:{type: String, require:[true, 'El Perfil Engineering es necesario']},//descriptora

  fecha_alta:{type: Date, require:[true, 'La fecha de alta es necesaria']},
  fecha_modificacion:{type: Date, require:false},
  usuario_alta:{type: String, require:[true, 'El usuario alta es necesario']},
  usuario_modificacion:{type: String, require:false}
});
personaSchema.plugin(aggregatePaginate);
module.exports = mongoose.model('persona',personaSchema);
