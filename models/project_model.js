var mongoose = require('mongoose');
var aggregatePaginate  = require('mongoose-aggregate-paginate-v2');

var Schema = mongoose.Schema;

var projectSchema = new Schema({
  codigo_sda_tool:{type: String},
  proyecto:{type: String},
  codigo_mvp:{type: String},
  mvp:{type: String},
  asignacion:{type: String},
  equipo:{type: String},
  perfil:{type: String}
});

projectSchema.plugin(aggregatePaginate);
module.exports = mongoose.model('project',projectSchema);