var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var projectSchema = new Schema({
  codigo_sda_tool:{type: String},
  proyecto:{type: String},
  codigo_mvp:{type: String},
  mvp:{type: String},
  asignacion:{type: String},
  equipo:{type: String},
  perfil:{type: String}
});

var persona_projectSchema = new Schema({
  registro:{type: String, require: [true, 'El registro es necesario']},
  proyectos:[projectSchema]
});

module.exports = mongoose.model('personsproject',persona_projectSchema);
