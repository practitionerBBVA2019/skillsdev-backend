var mongoose = require('mongoose');
var aggregatePaginate  = require('mongoose-aggregate-paginate-v2');
var Schema = mongoose.Schema;

var skillSchema = new Schema({
  codigo_skill:{type: String},
  estado:{type: String, require:[true, 'La disciplina es necesaria']},//descriptora
  descripcion_skill:{type: String},
  detalle_skill:{type: String},
  nivel_ingresado:{type: String},
  nivel_valorado:{type: String}
});

var persona_skillSchema = new Schema({
  registro:{type: String, require: [true, 'El registro es necesario']},
  skills:[skillSchema]
});

persona_skillSchema.plugin(aggregatePaginate);
module.exports = mongoose.model('personskill',persona_skillSchema);
