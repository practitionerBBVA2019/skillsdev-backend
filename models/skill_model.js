var mongoose = require('mongoose');
var aggregatePaginate  = require('mongoose-aggregate-paginate-v2');

var Schema = mongoose.Schema;

var skillSchema = new Schema({
  codigo_skill:{type: String},
  estado:{type: String, require:[true, 'La disciplina es necesaria']},//descriptora
  descripcion_skill:{type: String},
  detalle_skill:{type: String}
});

skillSchema.plugin(aggregatePaginate);
module.exports = mongoose.model('skill',skillSchema);