var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var usuarioSchema = new Schema({
  registro:{type: String, require: [true, 'El registro es necesario']},
  password:{type: String, require:[true, 'El password es necesario']}
});

module.exports = mongoose.model('usuario',usuarioSchema);
