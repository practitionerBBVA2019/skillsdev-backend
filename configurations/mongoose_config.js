const mongoose = require('mongoose');

//Conexion Cloud(Atlas)
mongoose.connect(process.env.STRING_CONECTION_DB, {dbName: process.env.DB_NAME}).then(
  ()=>{
    console.log('conecto a mongoDB puerto 27017: \x1b[32m%s\x1b[0m', 'Conexion Cloud exitosa!');
  }
).catch((err)=>console.error(err));


module.exports = {
    mongoose
}
