/**
 * Method to verify user's token
 */
var jwt = require('jsonwebtoken');

//Verifica token, siempre pasa por aquí antes de cualquier método(es otro middleware).
let tokenVerify = (req, res, next) => {

    let token = req.query.token;
    //let token = req.headers.token;

    jwt.verify(token, process.env.SEED, (err,decoded) =>{
        if(err){
            return res.status(401).json({
                ok:false,
                mensaje:'Token inválido',
                error:err
            });
        }

        next();//le permite continuar la execución, sino no funciona!
    });
}

module.exports = {
    tokenVerify
}
